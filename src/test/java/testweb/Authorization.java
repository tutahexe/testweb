package testweb;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Authorization extends Utilities {
	By signInHeaderButton = By.xpath("//a[@class='login']");
	By signOutHeaderButton = By.xpath("//a[@class='logout']");
	By emailInput = By.xpath("//input[@id='email']");
	By passwordInput = By.xpath("//input[@id='passwd']");
	By signInButton = By.xpath("//button[@id='SubmitLogin']");
	By loggedInIdentificator = By.xpath("//a[@class='account']");
	private String logoutUrlPart="index.php?mylogout=";
	private String email;
	private String password;
	private String baseUrl;

	@BeforeSuite
	public void before_suite() {
		loadProperties();
	}

	@BeforeMethod
	public void before() {
		startDriver();
	}

	@AfterMethod
	public void after() {
		stopDriver();
	}

	// This part is going be reused for all tests. To verify logout, user should be
	// logged in
	private void userLogin(WebDriver driver) {
		email = getEmail();
		password = getPassword();
		baseUrl = getBaseUrl();
		driver.get(baseUrl);
		driver.findElement(signInHeaderButton).click();
		driver.findElement(emailInput).sendKeys(email);
		driver.findElement(passwordInput).sendKeys(password);
		driver.findElement(signInButton).click();
		Assert.assertFalse(driver.findElements(signInButton).size()>0);;
	}

	// Test for Login functionality. User is able to pass authentication with valid
	// credentials, provided by config.
	@Test
	public void userIsAbleToSuccesfullyLogin() {
		WebDriver driver = getDriverInstance();
		userLogin(driver);
		String pageTitle = driver.getTitle();
		Assert.assertTrue(driver.findElement(loggedInIdentificator).isDisplayed());
		Assert.assertEquals(pageTitle, "My account - My Store");
	}

	// Test for Logout functionality. User is able to pass authentication with valid
	// credentials,and successfully log out.
	@Test
	public void userIsAbleToSuccesfullyLogout() {
		WebDriver driver = getDriverInstance();
		if (driver.findElements(signOutHeaderButton).size() == 0) {
			userLogin(driver);
		}
		driver.findElement(signOutHeaderButton).click();
		String pageTitle = driver.getTitle();
		Assert.assertFalse(driver.findElements(loggedInIdentificator).size()>0);
		Assert.assertEquals(pageTitle, "Login - My Store");
	}

	// Test for Logout link functionality. User is able to log out by following
	// link.
	@Test
	public void userIsAbleToSuccesfullyLogoutByLink() {
		WebDriver driver = getDriverInstance();
		if (driver.findElements(signOutHeaderButton).size() == 0) {
			userLogin(driver);
		}
		String logOutUrl = (baseUrl + logoutUrlPart);
		driver.get(logOutUrl);
		String pageTitle = driver.getTitle();
		Assert.assertFalse(driver.findElements(loggedInIdentificator).size()>0);
		Assert.assertEquals(pageTitle, "My Store");
	}
}
