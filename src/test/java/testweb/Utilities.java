package testweb;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Utilities {

	private static String browser;
	private static String baseUrl;
	private static String email;
	private static String password;
	private static String remote;
	private static String remote_hub_url;

	// Read properties file, saved parameters to variables
	public void loadProperties() {
		Properties prop = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream("src\\test\\resources\\config.properties");
			prop.load(fis);
			browser = prop.getProperty("browser");
			baseUrl = prop.getProperty("baseUrl");
			email = prop.getProperty("email");
			password = prop.getProperty("password");
			remote = prop.getProperty("remote");
			remote_hub_url = prop.getProperty("remote_hub_url");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	private static HashMap<Long, WebDriver> map = new HashMap<>();

	public static WebDriver getDriverInstance() {
		return map.get(Thread.currentThread().getId());
	}

	public void startDriver() {
		if (remote.equals("enabled")) {
			startRemoteDriver();
		} else {
			startLocalDriver();
		}
	}

	// Webdriver initialization point. Once driver is initialized it is stored in
	// map with key=id of current tread
	public void startLocalDriver() {
		WebDriver driver;
		if (browser.equals("firefox")) {
			// Side manager library is used to get rid of exe hardcoding. Downloads the
			// driver to '~/.cache/selenium'
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		if (browser.equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		} else {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		map.put(Thread.currentThread().getId(), driver);
	}

	// Remote driver is used for Selenium grid. All configuration can be found in cofig properties
	public void startRemoteDriver() {
		WebDriver driver;
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setBrowserName(browser);
		try {
			driver = new RemoteWebDriver(new URL(remote_hub_url), capabilities);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			map.put(Thread.currentThread().getId(), driver);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	// Search stored in map driver by key=id of current tread. If it's not null it
	// stops and then set to null
	public static void stopDriver() {
		WebDriver driver = map.get(Thread.currentThread().getId());
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}
}