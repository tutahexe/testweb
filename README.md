# TestWeb

TestWeb is selenium based automation framework used for testing 'automationpractice' website.

## Installation
Requirements:
* MAVEN
* JAVA 1.8
* Draw your attention that you don't need to download and configure driver.exe for local run due to driver manager library. It downloads exe file and stores it in '~/.cache/selenium' folder.
* Selenium grid for remote execution

Use MAVEN to install all dependencies.

```
mvn install
```


## Execution configuration
Go to 'src/test/resources/' and open 'config.properties'.
Set next parameters:
* Browser - chrome, firefox
* Email - email of user for authorization testing
* Password - password of user for authorization testing
* Remote - if 'enabled' remote driver will be initiated
* Remote_hub_url - Url for remote hub connection, used when remote is enabled

## Single thread / multi-thread configuration

Edit testng.xml to configure number of threads used in run.

##### For single thread run:.
testng.xml

```
<suite name="Suite">
```
##### For multi-thread run:
testng.xml

```
<suite name="Suite" parallel="methods" thread-count="n">
```
Where n - number of threads

## Local run

Use MAVEN to build and run tests.

```
mvn clean test
```

## Remote Run
This framework is built to run tests via selenium grid.
You can familiarize with Grid installation and requirements here:
[Setting up your own grid](https://www.selenium.dev/documentation/en/grid/grid_3/setting_up_your_own_grid/)
Once you have launched Hub - specify Remote_hub_url in config.properties.
Browser is used from config.properties also, while at this point desired platform is not specified.
Use MAVEN to build and run tests with remote launch in the same way as Local.

```
mvn clean test
```

## Report
Once test execution is finished - report can be found in '/test-output/index.html'.